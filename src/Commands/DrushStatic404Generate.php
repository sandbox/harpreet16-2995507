<?php

namespace Drupal\static_404\Commands;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\static_404\Commands
 * Note that when `drush static_404:static_404_generate` is called, the `-l [uri]` flag
 * must be passed for correct behavior. E.g.,
 *
 *  drush static_404:static_404_generate -l http://p2.takepart.com
 *
 * is a correct usage. Otherwise, css/js imports will break.

 */
class DrushStatic404Generate extends DrushCommands {

  /**
   * Drush command that displays the given text.
   *
   * @command static_404:static_404_generate
   * @aliases static-404
   * @usage static_404_generate
   */
  public function static_404_generate() {
    $boolean_generated = static_404_generate();
    if ($boolean_generated) {
      drush_print('Static 404 Generated');
    }
    else {
      drush_print('Error while creating Static 404 Generated');
    }
  }

}
