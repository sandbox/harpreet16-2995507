<?php

namespace Drupal\static_404\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Contains \Drupal\static_404\Controller\StaticPageController.
 */
class StaticPageController extends ControllerBase {
  public function static404Return() {
    $prefix = \Drupal::config('static_404.settings')->get('static_404_prefix');
    $prefix = (empty($prefix)) ? $prefix : '/' . $prefix;

    if (file_exists('public://404' . $prefix . '/index.php')) {
      include_once 'public://404' . $prefix . '/index.php';
      exit();
    }
    else {
      return array(
          '#type' => 'markup',
          '#markup' => t('Not Found'),
      );
    }
  }
}
